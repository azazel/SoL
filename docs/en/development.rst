.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 31 mar 2014 19:37:57 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2018 Lele Gaifax
..

=============
 Development
=============

SoL development is carried on in a git__ repository on GitLab__, that is automatically mirrored
on Bitbucket__.

You can visit the `activity`__ page to see what's happened recently.

If you are a developer, you are more than welcome to `fork it`__ and adapt or improve it to fit
your needs, and I will happily integrate back the changes you may contribute.

Alternatively you can translate it into another language, using the `online Weblate service`__.

There is also some `technical documentation`__ automatically extracted from the sources.

__ http://git-scm.com/
__ https://gitlab.com/lelix/SoL
__ https://bitbucket.org/lele/sol/
__ https://gitlab.com/lelix/SoL/activity
__ https://docs.gitlab.com/ee/workflow/forking_workflow.html
__ https://hosted.weblate.org/projects/sol/
__ ../index.html
