.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 11 nov 2008 23:07:33 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2009, 2010, 2013, 2014 Lele Gaifax
..

================
 Manuale utente
================

.. toctree::
   :maxdepth: 4

   interfacciautente
   stampati
   appendici

Indici e tabelle
================

* :ref:`genindex`
* :ref:`search`
