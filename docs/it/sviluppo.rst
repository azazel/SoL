.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 31 mar 2014 19:52:22 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2018 Lele Gaifax
..

==========
 Sviluppo
==========

Lo sviluppo di SoL avviene in un repository git__ ospitato da GitLab__, che viene
automaticamente sincronizzato su Bitbucket__.

Puoi visitare la pagina con `l'attività`__ per vedere cosa è stato fatto di recente.

Se sei uno sviluppatore, sei incoraggiato a `creare un fork`__ per adattare e migliorare SoL e
sarò più che contento di integrare le modifiche che potrai contribuire.

In alternativa puoi tradurlo in un'altra lingua, usando il `servizio online di Weblate`__.

C'è anche una `documentazione tecnica`__ estratta automaticamente dai sorgenti.

__ http://git-scm.com/
__ https://gitlab.com/lelix/SoL
__ https://bitbucket.org/lele/sol/
__ https://gitlab.com/lelix/SoL/activity
__ https://docs.gitlab.com/ee/workflow/forking_workflow.html
__ https://hosted.weblate.org/projects/sol/
__ ../index.html
