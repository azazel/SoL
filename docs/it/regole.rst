.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 07 apr 2009 13:10:37 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2009, 2010, 2014 Lele Gaifax
..

=====================================
 Le Regole Internazionali del Carrom
=====================================

Traduzione *libera* dall'originale in inglese delle `Official Laws of CARROM`__ pubblicate
della `Federazione Internazionale Carrom`__.

__ http://www.carrom.org/icf/?page=1&subcat=20
__ http://www.carrom.org/

.. warning:: Queste **non sono** le `Regole Ufficiali`__ della `Federazione Italiana Carrom`__,
             benché nella sostanza siano del tutto equivalenti: la differenze principali sono
             nella numerazione degli articoli e in piccole differenze nella formattazione del
             testo.

__ http://www.carromitaly.com/regoleFIC.html
__ http://www.carromitaly.com/

.. contents:: Sommario
   :local:

Glossario
=========

Nel contesto di questo Regolamento, i seguenti termini devono essere così interpretati:

.. glossary::

   Regole
     si intendono le “Regole del Carrom”.

   Tiro
     significa colpire una pedina con lo Striker, direttamente o indirettamente.

   Tiro corretto
     (o “proprio”) significa in accordo con le Regole.

   Tiro scorretto
     (o “improprio”) è da intendere contrario alle Regole.

   C/B
     indica il Carrom Board, cioè il Tavolo da Gioco.

   C/M
     è l'abbreviazione di Carrommen, le Pedine, sia al plurale che al singolare.

   Board
     si intende una singola giocata, dalla spaccata iniziale alla conclusione.

   Partita
     (o “game”) si indica una serie di massimo 8 board fino ad arrivare ai 25 punti.

   Incontro
     (o “match”) è da intendersi come una sfida al miglior risultato su tre game

   Spaccata
     (o “break”) indica il primo tiro di un board.

   Finire
     significa imbucare tutte le proprie pedine sul tavolo.

   Giocatore
     s'intende un Giocatore di Carrom.

   Piazzare
     indica il posizionamento, da parte di chi è autorizzato dalle Regole a farlo, delle pedine
     di penalità o pegno all'interno del Cerchio Esterno, in posizione piana. La Regina e/o le
     C/M che sono uscite dal tavolo devono sempre essere messe dall'Arbitro, a coprire il
     Cerchio Centrale.

   Imbucare
     significa spingere una pedina o la Regina in una delle buche, sia con un tiro corretto che
     con uno improprio.

   Spingere
     indica quando lo Striker viene mosso con un movimento brusco del gomito piuttosto che
     colpito con la punta del dito.

   Regina
     è la pedina rossa.

   Pegno
     è quando lo Striker finisce in buca, insieme a altre pedine o meno.

   Penalità
     indica una punizione per aver commesso un fallo violando una delle Regole.

   Conferma
     significa imbucare una propria pedina, nello stesso tiro o in quello immediatamente
     successivo, da parte di un giocatore che abbia messa in buca la Regina.

   Shot
     significa un :term:`pair` o un :term:`cannon`.

   Pair
     indica una coppia di pedine posizionate dentro il Cerchio Esterno, distanziate tra loro ma
     allineate verso una delle buche.

   Cannon
     indica una coppia di pedine a contatto tra di loro, posizionate nel Cerchio Esterno e
     allineate verso una delle buche.

   Thumbing
     indica un tiro fatto utilizzando il pollice.

   Turno
     significa avere il diritto di tirare.

   Giuria
     è la persona o gruppo di persone ufficialmente incaricate di dirimere eventuali
     controversie in un torneo.

   Arbitro
     è la persona ufficialmente incaricata di supervisionare e/o controllare un incontro o un
     torneo.

   Avversario nei Singoli
     è chi in quel determinato momento non ha il turno di gioco.

   Avversario nei Doppi
     è sia il giocatore seduto alla sinistra che quella alla destra di chi in quel momento ha
     il turno di gioco.

   Mano
     è da intendere la porzione che va dalle dita fino al polso della mano usata per giocare.

   Dito
     indica la porzione che comprende le prime due falangi del dito usato per giocare.

   Linee immaginarie
     si intendono i prolungamenti delle linee diagonali con le frecce che passano in mezzo ai
     Cerchi di Base.

   Slam bianco
     lo effettua chi riesce a imbucare tutte le pedine bianche e la Regina, nel rispetto delle
     Regole, al suo primo turno di gioco. Viene anche chiamato “Break to Finish”.

   Slam nero
     si intende il realizzare tutte le rimanenti pedine nere, inclusa o meno la Regina, al
     primo turno di gioco, rispettando ovviamente le Regole.

.. note:: Il singolare comprende il plurale, e il maschile comprende il femminile.


Posizione di gioco
==================

1. Nei Singoli, i giocatori si siedono uno di fronte all'altro.

2. Nei Doppi, i giocatori di ciascuna coppia giocano uno di fronte all'altro, occupando in tal
   modo tutti e quattro i lati.

3. La posizione assunta da un giocatore prima del suo turno di gioco può essere cambiata in
   qualunque momento, a patto che per tutta la durata del suo turno la posizione della sedia o
   dello sgabello su cui è seduto rimanga immutata.

4. Durante la partita:

  (a) nessuna parte del corpo dei giocatori, ad eccezione del braccio utilizzato per il tiro,
      può toccare il Tavolo o il sostegno su cui è appoggiato;

  (b) ciascun giocatore, durante il proprio turno di gioco, dovrà comunque assicurarsi che gli
      eventuali indumenti, anelli, braccialetti e orologi non tocchino la superficie di gioco.

5. È permesso l'utilizzo di qualsiasi materiale per alzare e/o aggiustare l'altezza della
   sedia, ma solo dopo la conclusione della partita.

6. Nessuna parte del corpo del giocatore, eccettuata la mano con cui tira, può superare le
   linee immaginarie diagonali.


Come si effettua il tiro
========================

7. Lo Striker deve essere colpito, non spinto.

8. Il tiro deve essere effettuato con un dito, con o senza il supporto delle altre dita.

9. Per giocare si può utilizzare una qualsiasi delle due mani.

10. Mentre si sta per tirare

   (a) la mano può toccare la superficie di gioco;

   (b) il gomito della mano utilizzata per giocare non deve toccare la superficie di gioco e
       non può oltrepassare le linee immaginarie;

   (c) la mano utilizzata, tuttavia, può superare le linee immaginarie;

   (d) non è permesso ancorarsi in alcun modo alla propria sedia, né tanto meno al sostegno del
       Tavolo, oppure tenere le gambe appoggiate all'eventuale rinforzo del sostegno.

   (e) l'avversario tuttavia potrà tenere le mani appoggiate sul proprio corpo o sulle gambe, o
       riposarsi appoggiandosi agli eventuali braccioli o schienale della sedia.


Lancio della moneta
===================

11. All'inizio di ogni incontro

   (a) l'Arbitro dovrà stabilire chi effettuerà la prima spaccata, lanciando una
       moneta oppure estraendo una pedina a caso;

   (b) il giocatore estratto potrà decidere su quale lato giocare oppure partire per primo:
       dovesse scegliere la posizione di gioco, dovrà comunicare la sua decisione all'Arbitro,
       che provvederà a far sedere per primo l'altro giocatore;

   (c) nei Doppi, la coppia estratta avrà la stessa possibilità di scelta come sopra;

   (d) se, invece, il giocatore estratto preferisse la spaccata, la scelta del posto di gioco
       spetta all'altro giocatore e quindi l'Arbitro farà sedere per primo il giocatore
       estratto;

   (e) una volta che i giocatori non estratti sono seduti, non possono scambiarsi ulteriormente
       di posto: questo medesimo ordine dovrà essere mantenuto per tutta la durata
       dell'incontro.


Partita di riscaldamento
========================

Possono essere effettuate due partite di riscaldamento, una per ogni giocatore o coppia. Tali
partite andranno giocate dopo che è stata fatta l'estrazione e prima di cominciare l'incontro
vero e proprio.


Spaccata
========

12. Prima della spaccata

   (a) le pedine devono essere disposte in posizione piana in modo tale che la Regina occupi il
       Cerchio Centrale e il resto delle pedine intorno ad essa:

      i. una prima cerchia alternando una pedina bianca a una nera

      ii. nella seconda, le pedine bianche formeranno una specie di “Y” con al centro la
          Regina, mentre lo spazio rimanente va riempito alternativamente di pedine bianche e
          nere

      iii. una volta sistemate, tutte le pedine devono toccarsi l'una con l'altra a formare un
           cerchio compatto all'interno del Cerchio Esterno: il giocatore può utilizzare le
           dita o lo Striker per far combaciare le pedine

   (b) questa disposizione delle pedine prima della spaccata deve essere effettuata con la
       minor perdita di tempo possibile alla fine di ciascuna partita.

13. La spaccata va effettuata dal giocatore che ha scelto di tirare per primo.

14. Il giocatore che esegue la spaccata avrà le pedine bianche, il suo avversario quelle nere,
    per tutta la durata di una singola partita. La Regina è in comune.

15. La spaccata deve essere effettuata solo dopo che l'Arbitro abbia dato il “Via”: da quel
    momento il gioco è considerato valido e il tiro deve essere effettuato entro 15 secondi
    dall'annuncio.

16. Se la spaccata viene effettuata prima che l'Arbitro abbia dato il “Via”, tutte le pedine e
    la Regina eventualmente imbucate vanno riposizionate sul Tavolo: il giocatore paga una
    pedina di penalità e perde il turno di gioco.

17. La spaccata

   (a) è da considerarsi effettuata se lo Striker tocca anche solo leggermente una qualsiasi
       pedina;

   (b) è da considerarsi non effettuata se lo Striker non tocca nessuna delle pedine nella sua
       corsa: in tal caso, il giocatore ha a disposizione al massimo altri due tentativi;

   (c) se dopo il numero consentito di tentativi nessuna pedina fosse ancora stata toccata, il
       giocatore perde il diritto di spaccare che passa all'avversario, il quale conserva le
       pedine nere ma non gli è permesso di risistemare le pedine sul tavolo; se anche
       l'avversario non riuscisse a effettuarla, la spaccata torna al primo giocatore e così
       via, fino a che non riesce;

   (d) se un giocatore, nel tentativo di spaccare, gioca un tiro improprio o imbuca il suo
       Striker senza aver toccato alcuna pedina, egli perde il turno senza sottostare a nessuna
       penalità.


Turno di gioco
==============

18. Fintantoché un giocatore imbuca le sue pedine e/o la Regina in accordo con le Regole, egli
    conserva il turno di gioco, altrimenti questo passa al suo avversario.

19. Turnazione:

    (a) Nei Singoli

        i. Nella prima partita, il giocatore che ha scelto di partire per primo ha le pedine
           bianche e il turno di spaccata passa alternativamente all'uno e all'altro giocatore
           durante l'incontro

        ii. nella seconda partita, esegue la spaccata per primo chi non l'ha effettuata nella
            prima partita

        iii. nella terza partita, la spaccata viene effettuata di nuovo dal primo giocatore

    (b) Nei Doppi, il turno passa al giocatore che siede alla destra di chi l'ha effettuata la
        volta precedente.

20. Un giocatore deve effettuare il suo tiro entro 15 secondi dal momento che lo Striker
    dell'avversario si è fermato ed è stato rimosso dal Tavolo, oppure da quando sono state
    posizionate le pedine di penalità.

21. Se un giocatore effettua il tiro quando non è il suo turno senza che l'Arbitro abbia modo
    di fermarlo, il giocatore in questione perde la partita delle pedine e Regina ancora
    presenti sul Tavolo.

    Se l'accaduto non viene notato da alcuno prima che il giocatore successivo abbia tirato,
    viene considerato valido e la partita prosegue normalmente.


Calcolo dei punti
=================

22. Il giocatore che per primo imbuca tutte le proprie pedine, vince la partita.

23. I punti sono calcolati come segue:

   (a) Regina: 3 punti fino al raggiungimento dei 21 punti inclusi;

   (b) Pedine: 1 punto per ogni pedina;

   (c) il numero delle pedine avversarie ancora sul Tavolo darà il punteggio ottenuto da quel
       giocatore in quella partita;

   (d) al giocatore vengono accreditati i punti della Regina solo in caso egli vinca la
       partita;

   (e) il giocatore che perde la partita non otterrà alcun punteggio dalla Regina, anche se
       l'avesse regolarmente imbucata e confermata.

24. Il giocatore perde la possibilità di avvantaggiarsi dei 3 punti della Regina quando
    raggiunge i 22 punti.

25. Il punteggio massimo realizzabile in una partita è 12:  eventuali pegni o penalità
    dell'avversario devono essere ignorati.


Durata degli incontri
=====================

26. Un incontro termina ai 25 punti oppure dopo otto partite: il giocatore che raggiunge per
    primo i 25 punti o che è in vantaggio alla conclusione dell'ottavo board è il vincitore
    dell'incontro.

27. Nei tornei a gironi

   (a) fino agli ottavi di finale inclusi, ogni incontro viene disputato sulla base di otto
       board;

   (b) se al termine dell'ottavo board si fosse in condizione di parità, si giocherà un
       ulteriore partita per stabilire il vincitore;

   (c) prima di giocare lo spareggio si dovrà stabilire, tirando a sorte, il giocatore con
       diritto di spaccare per primo;

28. La finale sarà decisa al meglio di tre incontri.


Cambio campo
============

29. Nei Singoli, i giocatori si devono scambiare il posto alla fine di ogni partita.

30. Nei Doppi, il cambio viene effettuato spostando ogni giocatore di un posto alla sua destra,
    alla fine di ogni incontro.

31. Nei tornei a gironi

   (a) fino agli ottavi di finale inclusi, il cambio campo deve essere effettuato dopo la
       quarta partita o appena uno dei giocatori/delle coppie raggiunge i 13 punti;

   (b) dai quarti di finale in avanti il cambio campo va effettuato nel momento in cui uno dei
       giocatori/delle coppie raggiunge i 13 punti;

   (c) il cambio di campo, se passa inosservato sia dall'Arbitro sia dai giocatori, dovrà
       essere fatto nel momento in cui viene notata la mancanza, dopo la conclusione della
       partita in corso.

32. I giocatori non devono impiegare più di due minuti per scambiarsi il posto.


Falli
=====

33. In generale, qualsiasi violazione delle Regole o qualsiasi cosa fatta in contrasto con
    quanto specificato o comunque inteso dalle presenti Regole, dovrà essere classificato in:

   (a) Fallo tecnico
   (b) Fallo

Falli tecnici
-------------

34. Verrà detta “fallo tecnico”

   (a) qualsiasi violazione delle Regole commessa da un giocatore prima del suo primo tiro al
       suo turno di gioco: tale violazione comporta il riposizionamento, da parte
       dell'avversario, di una delle pedine imbucate dal giocatore che poi potrà proseguire il
       suo gioco;

   (b) qualsiasi violazione delle Regole commessa dall'avversario (cioè dal giocatore che non
       ha in quel momento il turno di gioco) e subirà la stessa penalità imposta nel punto
       precedente.

Falli
-----

35. Qualunque violazione delle Regole commessa da un giocatore durante o dopo il suo primo
    tiro, ad ogni turno di gioco, viene chiamata “fallo” che comporta il riposizionamento,
    da parte dell'avversario, di una delle pedine imbucate dal giocatore e la perdita per
    quest'ultimo del turno di gioco.

36. Se il fallo viene commesso imbucando una pedina o la Regina, tutte le pedine imbucate,
    compresa l'eventuale Regina, verranno riposizionate sul tavolo e il turno di gioco passa la
    giocatore successivo.


Pedine fuori dal tavolo
=======================

37. Qualora una pedina, o la Regina, dovesse uscire dalla superficie di gioco dovrà essere
    rimesse dall'Arbitro a coprire il Cerchio Centrale o, se lo spazio non lo permette, a
    occuparne la maggior parte possibile.

38. Dovessero uscire contemporaneamente una pedina insieme alla Regina, la precedenza va a
    quest'ultima che dovrà pertanto essere posizionata per prima, mentre l'altra pedina andrà
    messa a contatto con la Regina, sul lato opposto al giocatore che ha il turno di gioco.

39. Se invece nello stesso tiro escono una pedina bianca e una nera, la precedenza va alla
    pedina del giocatore che ha effettuato il tiro, l'altra posizionata a contatto nella stessa
    maniera descritta nel punto precedente.

40. Quando escono dal Tavolo più di due pedine, le prime due devono essere posizionate seguendo
    le direttive esposte nei punti precedenti, le altre disposte in modo da toccarle, per
    quanto possibile.

41. Se una pedina, o la Regina, dovesse uscire dalla superficie di gioco e ricadervi

   (a) quella pedina o la Regina dovrà essere riposizionata sul Cerchio Centrale secondo le
       Regole dall'Arbitro che potrà, a sua discrezione, ripristinare la posizione delle altre
       pedine eventualmente coinvolte;

   (b) tuttavia, se la pedina, o la Regina, dovesse rientrare sul Tavolo dopo aver urtato
       contro il sostegno della lampada, o contro la lampada stessa, questo verrà considerato
       come un suo movimento naturale, e le pedine eventualmente coinvolte dovranno rimanere
       dove sono.


Pedine che rotolano e si sovrappongono
======================================

42. Se una pedina o la Regina rimane verticale sul suo bordo, dovrà essere lasciata in quella
    posizione.

43. Se due pedine e/o la Regina si sovrappongono, dovranno essere lasciate come stanno.

44. Qualora lo Striker si fermasse su una pedina o sulla Regina

   (a) l'Arbitro dovrà rimuovere lo Striker senza disturbare la posizione della pedina;

   (b) se la pedina dovesse muoversi nell'intento, l'Arbitro dovrà ripristinarne, per quanto
       possibile, la posizione originale;

   (c) se questo succede sull'orlo di una buca e nel rimuovere lo Striker la pedina o la Regina
       dovessero perdere il loro centro di gravità e cadere nella buca, verrà considerata come
       propriamente imbucata.

45. Se una pedina o la Regina si ferma sopra lo Striker

   (a) l'Arbitro dovrà rimuoverlo alzando la pedina e poi riappoggiandola, per quanto
       possibile, come se lo Striker non fosse stato là;

   (b) se però questo succedesse sull'orlo di una buca e rimuovendo la pedina o la Regina lo
       Striker, perdendo il proprio centro di gravità dovesse cadervi, verrà considerato come
       imbucato: il giocatore dovrà pagare una penalità come previsto dalle Regole.

46. Se una pedina, in precedenza ferma sull'orlo di una buca, vi cade per qualsiasi ragione,
    verrà considerata propriamente imbucata.


Pegni e penalità
================

47. Se un giocatore imbuca il suo Striker, sia con un tiro corretto che con uno improprio

   (a) egli perderà il turno di gioco e dovrà pagare una pedina di penalità, che dovrà essere
       posizionata dall'avversario: questa pedina viene chiamata “Pegno”;

   (b) qualora il giocatore in questione non avesse ancora imbucato nessuna delle sue pedine,
       la penalità rimarrà in sospeso finché non si renda disponibile una pedina

      i. il pegno o la penalità dovranno essere rimesse in gioco non appena si rendano
         disponibili, dopo la conclusione del tiro ma eventualmente nel turno di gioco dello
         stesso giocatore

      ii. nei Doppi la penalità va rimessa in gioco dal giocatore che siede alla destra di chi
          a il turno di gioco al momento che la pedina si rende disponibile

      iii. se però, durante il turno di un giocatore, questo imbucasse delle pedine
           dell'avversario, magari insieme alle proprie, rendendo così disponibile la penalità,
           dovrà essere il giocatore stesso a estrarla e riposizionarla.

48. Se un giocatore imbuca il suo Striker insieme a una o più delle proprie pedine, queste
    dovranno essere riposizionate sul Tavolo, più una di penalità

   (a) nel caso tiro corretto, il giocatore mantiene il turno di gioco;

   (b) in caso di tiro improprio, il turno passa all'avversario.

49. Quando un giocatore imbuca il proprio Striker insieme a pedine dell'avversario, queste
    verranno considerate come fatte: verrà applicata la pedina di pegno prevista e il giocatore
    perde il turno di gioco.

50. Qualora lo Striker finisse in buca insieme a pedine sia proprie che avversarie, dovranno
    essere riposizionate, a cura dell'avversario, solo quelle del giocatore, più una di
    penalità, quindi il giocatore mantiene il turno di gioco.

51. Se un giocatore imbuca delle pedine dell'avversario con un tiro improprio, verranno
    considerate regolarmente realizzate, il giocatore subirà una pedina di penalità posizionata
    dall'avversario e perderà il turno di gioco.

52. Quando un giocatore imbuca le sue pedine con un tiro improprio, quelle stesse pedine più
    una di penalità dovranno essere rimesse in gioco dall'avversario, al quale passerà anche il
    turno di gioco.

53. Se la pedina di penalità è disponibile ma non c'è spazio a sufficienza per piazzarla, è
    consentito a chi deve farlo di posticipare finché si liberi lo spazio necessario.

   (a) Nei Doppi, qualora un giocatore decida di posticipare il piazzamento passando di fatto
       la responsabilità al proprio partner, solo quest'ultimo avrà diritto a farlo.

54. Se lo spazio necessario al posizionamento della penalità fosse disponibile ma il giocatore
    al quale spetta farlo non volesse rischiare un fallo, egli dovrà rinunciarvi e la penalità
    verrà annullata.

55. Quando si renda disponibile lo spazio necessario al posizionamento della penalità durante
    il turno di un giocatore autorizzato a farlo, la pedina andrà piazzata immediatamente.

56. Se il giocatore che piazza la penalità si dovesse sbagliare ponendo sul Tavolo delle
    proprie pedine anziché quelle del giocatore che ha commesso il fallo

  (a) se questo viene fatto notare dall'Arbitro o da uno dei giocatori, la situazione dovrà
      essere rettificata e, avendo commesso fallo, dovrà subire la penalità prevista dalle
      Regole;

  (b) se la cosa non viene notata dall'Arbitro o da uno degli avversari prima che sia
      effettuato il tiro successivo, le pedine riposizionate verranno considerate valide e
      regolari.

57. Se un giocatore è in debito di più di una pedina di penalità, le pedine disponibili devono
    essere rimesse immediatamente in gioco, le altre non appena vengano imbucate.

58. Il posizionamento va considerato concluso non appena il giocatore stacca il dito dalla
    pedina, ammesso che questa si trovi dentro il Cerchio Esterno.

    Non è consentito inoltre trattenere in mano altre pedine o lo Striker mentre si posiziona
    la penalità.

59. Quando un giocatore pone o sposta la penalità fuori dal Cerchio Esterno, gli verrà chiesto
    di metterla al suo interno e viene dichiarato fallo, con le conseguenze previste dalle
    Regole.

60. Nel piazzare la pedina di penalità, il giocatore non deve muovere o disturbare nessun'altra
    pedina o la Regina: se questo accadesse, l'Arbitro dovrà ripristinare per quanto possibile
    le posizioni originali e dichiarare fallo nei confronti del giocatore, con le conseguenze
    previste dalle Regole.

61. Un giocatore può condonare una penalità all'avversario, a patto che si tratti della sua
    totalità e non solo di una parte: questa decisione va comunicata all'Arbitro entro 15
    secondi, scaduti i quali il diritto a piazzare la penalità decade.

62. Il limite di tempo entro il quale posizionare la penalità, dopo l'annuncio fatto
    dall'Arbitro, è di 15 secondi.

63. Le penalità non potranno essere posizionate direttamente a contatto con pedine già presenti
    e in particolare non è ammesso formare uno `shot` con la Regina: quando accadesse,
    l'Arbitro dovrà richiamare il giocatore a spostare la pedina, dichiarandogli fallo.

64. Le pedine di pegno o penalità, quando vengono posizionate, non devono coprire il Cerchio
    Centrale nemmeno parzialmente: se questo dovesse accadere, si chiederà al giocatore di
    aggiustarne la posizione e verrà dichiarato fallo nei suoi confronti.

65. Nei Doppi, un giocatore non deve estrarre le pedine di penalità quando a sistemarle deve
    essere il suo partner: nel caso in cui quest'ultimo non trovasse le pedine nelle buche
    vicine a lui, le dovrà chiedere all'Arbitro, specificandone il numero e il colore.

66. Se nel corso della partita un giocatore si alzasse per qualsiasi ragione dal suo posto, sia
    durante il suo turno che quello dell'avversario, perderà il game di quante sono le sue
    pedine, più l'eventuale Regina, ancora sul Tavolo. Se il punteggio dell'avversario è di 22
    punti o più, la Regina non deve essere conteggiata.


Regina
======

67. Un giocatore ha diritto a imbucare la Regina e quindi a confermarla quando almeno una delle
    sue pedine sia già stata imbucata.

68. La Regina deve essere piazzata dall'Arbitro esclusivamente sul Cerchio Centrale. Se nel
    piazzarla si dovesse formare un shot non potrà essere alterato.

69. Se il Cerchio Centrale fosse parzialmente o completamente coperto da altre pedine, la
    Regina dovrà essere piazzata in modo da occuparne quanto più possibile la porzione
    disponibile, oppure in una posizione adiacente al Cerchio Centrale in maniera tale che non
    sia facilmente realizzabile dal giocatore di turno: in tali casi, la decisione definitiva
    spetta all'Arbitro.

70. Quando la Regina viene imbucata prima di aver realizzato almeno un'altra pedina, la Regina
    va riposizionata al centro e il giocatore perde il turno.

71. Se un giocatore imbuca la Regina quando è in debito di una penalità, la Regina va rimessa
    al centro e il giocatore perde il turno.

72. Se, durante la spaccata o in un tiro successivo quando tutte le pedine del giocatore sono
    ancora sul Tavolo, viene imbucata la Regina insieme allo Striker, la Regina va rimessa al
    centro, il giocatore paga una pedina di penalità e perde il turno.

73. Se la Regina viene imbucata ma non viene confermata al tiro successivo, deve essere rimessa
    in gioco. Se però questo non viene notato né dall'Arbitro né dall'avversario prima che sia
    effettuato il tiro successivo, la Regina viene considerata regolarmente confermata.

74. Quando la Regina viene imbucata contemporaneamente a una pedina del giocatore, viene
    considerata confermata.

    (a) Se però questo succede durante la spaccata, o in un tiro successivo quando tutte le
        pedine del giocatore sono ancora in gioco, la Regina deve essere confermata. Tuttavia
        se più di una pedina fosse stata imbucata insieme alla Regina, questa è da considerare
        confermata.

75. Per i successivi casi, se la situazione si verifica in seguito ad un tiro improprio, il
    giocatore oltre alla penalità indicata perde in ogni caso il turno di gioco

    (a) se la Regina, una pedina e lo Striker finissero in buca contemporaneamente, sia la
        Regina che la pedina, più una di penalità, dovranno essere rimesse in gioco e il
        giocatore conserva il turno;

    (b) qualora un giocatore imbucasse la Regina insieme allo Striker, la Regina e una sua
        pedina di penalità vanno rimesse in gioco dall'Arbitro e il giocatore mantiene il
        turno;

    (c) se nel tentare di confermare la Regina venisse imbucato lo Striker da solo, la Regina
        va rimessa in gioco, il giocatore paga una pedina di pegno e perde il turno;

    (d) Se tentando di confermare la Regina il giocatore imbuca lo Striker insieme a una delle
        sue pedine, quest'ultime più una di penalità dovranno essere rimesse in gioco e il
        giocatore conserverà il turno: nel caso in cui non riuscisse a confermarla nel
        successivo tiro, la Regina verrà rimessa al centro del Tavolo.

76. Nei seguenti casi, il giocatore che effettua il tiro vince la partita, aggiudicandosi i
    soli punti della Regina, che vale 3 fino al raggiungimento dei 21 punti, superati i quali
    vale un solo punto. Se però si verificassero in seguito a un tiro improprio, è l'avversario
    ad aggiudicarsi la partita con il medesimo punteggio: in tal caso l'avversario potrà
    avvalersi del diritto di farsi conteggiare anche la pedina di pegno del giocatore che ha
    commesso il fallo, facendone esplicita richiesta

    (a) Confermando la Regina, vengono imbucate contemporaneamente le ultime pedine di entrambi
        i giocatori

    (b) Nello stesso tiro vanno in buca la Regina l'ultima pedina del giocatore e anche
        l'ultima dell'avversario.

77. Se un giocatore imbuca la sua ultima pedina quando la Regina è ancora sul tavolo,
    l'avversario si aggiudica l'incontro di 3 punti se non ha ancora raggiunto i 21 punti,
    altrimenti di un solo punto.

78. Nei casi che seguono è sempre l'avversario ad aggiudicarsi la partita del numero di pedine
    ancora sul tavolo più la Regina, se presente; egli potrà o meno esigere il pagamento della
    pedina di pegno dovuta in caso di tiro irregolare, come nel punto precedente. La Regina
    viene conteggiata normalmente fino al raggiungimento dei 21 punti (del giocatore che se
    l'aggiudica), superati i quali non deve essere inclusa nel punteggio, salvo quando ci sia
    la sola Regina sul tavolo, nel qual caso vale un singolo punto come una normale pedina

    (a) tentando di confermare la Regina, si dovesse imbucare l'ultima pedina dell'avversario;

    (b) quando un giocatore imbuca l'ultima pedina dell'avversario mentre la Regina è ancora
        sul Tavolo.

79. Se la Regina finisse per posizionarsi sull'orlo di una buca e in seguito, per qualsiasi
    ragione, vi cadesse dentro, va considerata regolarmente imbucata.


Generale
========

80. Il tiro può essere effettuato solo utilizzando uno Striker, che dovrà essere presentato dal
    giocatore e certificato dall'Arbitro prima dell'inizio dei giochi.

81. Se mentre effettua il tiro lo Striker del giocatore dovesse uscire dal tavolo imbucando al
    contempo proprie pedine e/o la Regina, il giocatore conserverà il turno.

82. È consentito cambiare il proprio Striker con un altro, debitamente controllato e approvato
    dalla Giuria, solo alla conclusione della partita. Tuttavia, se durante il gioco lo Striker
    dovesse rompersi, è permessa la sua sostituzione al termine del tiro corrente.

83. Una volta posizionato, la posizione del tavolo deve rimanere inalterata per tutta la durata
    dell'incontro. La sola e unica autorità che può cambiarne la posizione, dopo un attento
    esame dietro appello di uno dei giocatori, è l'Arbitro.

84. Il Tavolo o il supporto su cui è appoggiato destinato a un particolare incontro non potrà
    essere sostituito se non al termine di una partita. L'Arbitro è l'unica autorità che può
    prendere decisioni sulle condizioni del Tavolo o delle sedie.

85. Qualsiasi pedina rovinata di cui sia stata accettata la sostituzione, sarà rimpiazzata
    dall'Arbitro mantenendone la stessa posizione per quanto possibile.

86. Il conteggio del tempo viene fermato quando uno dei giocatori si appella all'Arbitro. Il
    gioco non può continuare fino a che l'Arbitro abbia dato il “Via”.

   (a) Chi non osserva questa regola perde immediatamente la partita di tanti punti quante sono
       le sue pedine ancora sul Tavolo, più quelli eventuali della Regina, secondo le Regole,
       se fosse ancora in gioco.

87. La polvere deve essere distribuita uniformemente dal giocatore di turno subito prima della
    spaccata. La quantità di polvere non dovrà essere né eccessiva né troppo scarsa.

   (a) La polvere non può essere più aggiunta o rimossa una volta che l'Arbitro abbia dato il
       “Via”.

   (b) Non è permesso alcun tentativo volontario di rimuovere o spostare la polvere dalla
       superficie di gioco.

   (c) Non è permesso tentare di rimuoverla nemmeno soffiandovi sopra, o in qualsiasi altra
       maniera.

   (d) Tuttavia, il giocatore di turno può, utilizzando il suo Striker, rimuovere e/o sistemare
       l'eccessiva polvere che fosse presente lungo le sue Linee di Base o attorno ai propri
       Cerchi di Base.

   (e) L'Arbitro ha il diritto di controllare l'uniformità della distribuzione della polvere
       sulla superficie del Tavolo.

   (f) Si deve richiedere all'Arbitro la rimozione di cenere, insetti e quant'altro fosse
       presente sul Tavolo durante la partita.

88. Un tiro è da considerare concluso solo quando sia lo Striker sia le pedine, Regina
    compresa, da esso movimentate si fermano.  Lo Striker deve essere rimosso immediatamente
    dopo dal giocatore. L'Arbitro può prestare assistenza, se richiesto.

89. Il conteggio del tempo comincia

   (a) non appena il tiro è completato, finché il giocatore continua a imbucare le sue pedine
       e/o la Regina;

   (b) dal momento in cui rimuove il suo Striker dal Tavolo, quando il giocatore non riesce a
       imbucare alcunché.

90. Un giocatore non può urtare, tamburellare o comunque disturbare il Tavolo di gioco, sia
    di proposito che inavvertitamente

  (a) se questo accadesse deve essere dichiarato fallo nei confronti del giocatore e l'Arbitro
      dovrà ripristinare la posizione originale delle pedine;

  (b) se l'intervento del giocatore è tale che la posizione originale delle pedine non possa
      essere ripristinata, egli perde la partita di tanti punti quante sono le sue pedine
      ancora sul Tavolo, più quelli eventuali della Regina, secondo quanto prescritto dalle
      Regole.

91. Un tiro è da considerare effettuato non appena lo Striker non è più a contatto delle Linee
    di Base o dei Cerchi di Base, indipendentemente dal fatto di aver toccato o meno delle
    pedine.

    Se invece lo Striker non esce dalle Linee di Base o dai Cerchi di Base e non tocca nessuna
    pedina, il tiro non va considerato come effettuato.

92. Un giocatore può chiedere all'Arbitro il punteggio dell'incontro solo durante il suo turno
    di gioco.

93. Un giocatore non deve distrarre l'avversario in nessuna maniera.

94. Quando si sta per tirare

  (a) lo Striker deve toccare entrambe le Linee di Base;

  (b) se il tiro viene effettuato a partire da uno dei Cerchi di Base, lo Striker lo deve
      coprire completamente ma non deve toccare le frecce diagonali.

95. Nei Doppi, durante la partita non è consentito ai partner di parlare o comunicare anche a
    gesti. Qualunque violazione di questa regola è trattata come un Fallo e punita secondo le
    Regole.

96. Ai giocatori non è permesso di parlare con gli spettatori senza il permesso dell'Arbitro.

97. Durante il loro turno di gioco, i giocatori non possono tenere in mano nessun materiale
    solido, eccetto lo Striker.

98. Le pedine e la Regina non devono essere mosse o disturbate, dopo la spaccata, se non con un
    tiro: se vengono toccate, spostate o disturbate dopo la spaccata, la loro posizione
    originale deve essere per quanto possibile ripristinata dall'Arbitro, che dichiarerà fallo
    nei confronti del giocatore che ha commesso il fatto.

99. Non è permesso fare delle prove sul Tavolo con lo Striker durante il corso della partita.

100. Durante la partita, i giocatori non devono tenere i loro Striker sul bordo del Tavolo, o
     in una delle buche.

     Tuttavia al giocatore che deve posizionare la penalità è consentito, mentre effettua
     l'operazione, di appoggiare lo Striker sul bordo, esclusivamente sulla sponda vicina alle
     proprie Linee di Base.

101. Se i giocatori si passano la mano per tre volte consecutive ognuno, la partita viene
     annullata e deve essere rigiocata.

102. Deve esserci un intervallo di dieci minuti tra il secondo e il terzo game.

103. È consentito a ogni giocatore di concedere l'incontro o la partita in qualunque momento.

104. Se, per motivi imprevisti fuori dal controllo dell'Arbitro fosse impossibile ripristinare
     la posizione delle pedine sul Tavolo, quella particolare partita andrà rigiocata.

105. Se si dovesse rompere una pedina durante una partita, farà fede la posizione del pezzo più
     grande, e l'Arbitro avrà la parola definitiva.

106. Nell'eventualità che sia le Linee di Base che i Cerchi di Base risultassero occupati dalle
     pedine in modo tale da rendere impossibile a un giocatore il posizionamento dello Striker
     per tirare, il board dovrà essere rigiocato.


Perdita dell'incontro
=====================

107. Quando un giocatore commette uno qualsiasi di questi atti di indisciplina viene dichiarato
     perdente dell'intero incontro dall'Arbitro:

     (a) lascia il proprio posto senza il permesso dell'Arbitro;

     (b) non rispetta il limite di tempo per il riposo previsto prima della partita decisiva di
         un incontro;

     (c) non rispetta le decisioni dell'Arbitro o della Giuria;

     (d) insulta l'arbitro prima, durante o immediatamente dopo l'incontro;

     (e) fuma durante l'incontro, o gioca sotto l'influenza di bevande o droghe intossicanti;

     (f) gioca con uno striker non certificato;

     (g) smette di giocare durante l'incontro senza il permesso dell'Arbitro;

     (h) disturba o distrae l'attenzione dell'avversario più di due volte durante il turno
         dell'avversario stesso, ignorando i richiami dell'Arbitro;

     (i) non si presenta all'Arbitro entro 15 minuti da quando è stato annunciato l'incontro.


Proteste
========

108. Qualsiasi protesta deve essere presentata, scritta in Inglese e firmata dal giocatore o
     dal Capitano della squadra, alla Segreteria del Torneo, per tramite dell'Arbitro.

109. Le proteste orali dovranno essere immediatamente rese note all'Arbitro, insieme al
     pagamento della tassa prevista. Nessuna protesta sarà accettata senza il pagamento della
     tassa.

110. Le proteste scritte devono essere presentate entro 15 minuti dalla conclusione della
     partita in questione.

111. La protesta dovrà descrivere la situazione occorsa, la decisione presa dall'Arbitro e
     l'obbiezione sollevata dalla parte in causa.

112. Le proteste formulate con un linguaggio volgare e inappropriato saranno rigettate e
     daranno seguito a azioni disciplinari.

113. Una volta che la protesta è stata presentata, non è permesso ritirarla.

114. Dopo che la protesta è stata presentata e accolta, il gioco dovrà continuare da dove era
     stato sospeso. Il risultato dell'incontro non sarà annunciato finché la Giuria non emette
     il verdetto sulla protesta.

115. Il verdetto della Giuria deve essere comunicato alle parti in causa entro un'ora da quando
     è stata presentata la protesta

     (a) se il verdetto è a favore, l'incontro dovrà continuare rigiocando dall'inizio la
         partita che si stava effettuando quando è stata presentata la protesta e la tassa
         verrà restituita;

     (b) se invece la Giuria non accetta la protesta, il risultato dell'incontro rimane
         immutato e la tassa viene trattenuta.
