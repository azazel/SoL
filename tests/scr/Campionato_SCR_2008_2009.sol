clubs:
- {couplings: dazed, description: Scarambol Club Rovereto, emblem: scr.png}
players:
- {club: 1, firstname: Emanuele, lastname: Gaifas, nationality: ITA, nickname: lele,
  portrait: lele.png, sex: null}
- {club: 1, firstname: Stefania, lastname: Andreolli, nationality: ITA, nickname: '',
  portrait: null, sex: F}
- {club: 1, firstname: Paolo, lastname: Cominelli, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Flavio, lastname: Pizzinat, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Fabio, lastname: Tomasi, nationality: ITA, nickname: '', portrait: null,
  sex: null}
- {club: 1, firstname: Stefano, lastname: Galvagni, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Juri, lastname: Golin, nationality: ITA, nickname: Picol, portrait: null,
  sex: null}
- {club: 1, firstname: Giannantonio, lastname: Maninfior, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Sandro, lastname: Sartori, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Maurizio, lastname: Sassosi, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Devid, lastname: ., nationality: ITA, nickname: '', portrait: null,
  sex: null}
- {club: 1, firstname: Fabrizio, lastname: Festi, nationality: ITA, nickname: '',
  portrait: null, sex: null}
- {club: 1, firstname: Giordano, lastname: Chizzola, nationality: ITA, nickname: '',
  portrait: null, sex: null}
seasons:
- {club: 1, description: Campionato SCR 2008-2009, playersperteam: 1, previous: Campionato
    SCR 2007-2008, prizefactor: '1', prizes: fixed, skipworstprizes: 0}
---
competitors:
- bucholz: 60
  netscore: 53
  players: [1]
  points: 12
  prize: '18'
  retired: false
  totscore: 86
- bucholz: 38
  netscore: 1
  players: [2]
  points: 8
  prize: '10'
  retired: false
  totscore: 56
- bucholz: 52
  netscore: 44
  players: [3]
  points: 10
  prize: '13'
  retired: false
  totscore: 96
- bucholz: 52
  netscore: 11
  players: [4]
  points: 6
  prize: '7'
  retired: false
  totscore: 66
- bucholz: 64
  netscore: -24
  players: [5]
  points: 6
  prize: '8'
  retired: false
  totscore: 57
- bucholz: 48
  netscore: 2
  players: [6]
  points: 4
  prize: '6'
  retired: false
  totscore: 64
- bucholz: 60
  netscore: 21
  players: [7]
  points: 10
  prize: '14'
  retired: false
  totscore: 82
- bucholz: 46
  netscore: 3
  players: [8]
  points: 8
  prize: '12'
  retired: false
  totscore: 65
- bucholz: 42
  netscore: 34
  players: [9]
  points: 8
  prize: '11'
  retired: false
  totscore: 69
- bucholz: 56
  netscore: 42
  players: [10]
  points: 12
  prize: '16'
  retired: false
  totscore: 92
- bucholz: 36
  netscore: 33
  players: [11]
  points: 8
  prize: '9'
  retired: false
  totscore: 68
- bucholz: 48
  netscore: -10
  players: [12]
  points: 4
  prize: '5'
  retired: false
  totscore: 55
- bucholz: 44
  netscore: -35
  players: [13]
  points: 2
  prize: '4'
  retired: false
  totscore: 34
couplings: dazed
currentturn: 7
date: 2008-10-25
description: "1\xB0 Torneo"
location: Sede SCR
matches:
- {board: 1, competitor1: 3, competitor2: 13, score1: 25, score2: 0, turn: 1}
- {board: 2, competitor1: 9, competitor2: 5, score1: 8, score2: 11, turn: 1}
- {board: 3, competitor1: 11, competitor2: 10, score1: 6, score2: 11, turn: 1}
- {board: 4, competitor1: 2, competitor2: 8, score1: 0, score2: 14, turn: 1}
- {board: 5, competitor1: 6, competitor2: null, score1: 25, score2: 0, turn: 1}
- {board: 6, competitor1: 12, competitor2: 7, score1: 0, score2: 13, turn: 1}
- {board: 7, competitor1: 4, competitor2: 1, score1: 5, score2: 14, turn: 1}
- {board: 1, competitor1: 7, competitor2: 9, score1: 9, score2: 8, turn: 2}
- {board: 2, competitor1: 12, competitor2: null, score1: 25, score2: 0, turn: 2}
- {board: 3, competitor1: 4, competitor2: 13, score1: 5, score2: 4, turn: 2}
- {board: 4, competitor1: 8, competitor2: 5, score1: 2, score2: 14, turn: 2}
- {board: 5, competitor1: 3, competitor2: 1, score1: 7, score2: 11, turn: 2}
- {board: 6, competitor1: 6, competitor2: 10, score1: 4, score2: 18, turn: 2}
- {board: 7, competitor1: 11, competitor2: 2, score1: 2, score2: 8, turn: 2}
- {board: 1, competitor1: 8, competitor2: 6, score1: 14, score2: 9, turn: 3}
- {board: 2, competitor1: 11, competitor2: null, score1: 25, score2: 0, turn: 3}
- {board: 3, competitor1: 3, competitor2: 4, score1: 13, score2: 6, turn: 3}
- {board: 4, competitor1: 1, competitor2: 5, score1: 15, score2: 2, turn: 3}
- {board: 5, competitor1: 10, competitor2: 7, score1: 11, score2: 6, turn: 3}
- {board: 6, competitor1: 9, competitor2: 13, score1: 10, score2: 1, turn: 3}
- {board: 7, competitor1: 12, competitor2: 2, score1: 17, score2: 2, turn: 3}
- {board: 1, competitor1: 13, competitor2: null, score1: 25, score2: 0, turn: 4}
- {board: 2, competitor1: 3, competitor2: 6, score1: 6, score2: 4, turn: 4}
- {board: 3, competitor1: 2, competitor2: 9, score1: 1, score2: 8, turn: 4}
- {board: 4, competitor1: 4, competitor2: 11, score1: 13, score2: 0, turn: 4}
- {board: 5, competitor1: 5, competitor2: 12, score1: 11, score2: 1, turn: 4}
- {board: 6, competitor1: 7, competitor2: 8, score1: 14, score2: 5, turn: 4}
- {board: 7, competitor1: 1, competitor2: 10, score1: 16, score2: 1, turn: 4}
- {board: 1, competitor1: 8, competitor2: 13, score1: 11, score2: 4, turn: 5}
- {board: 2, competitor1: 10, competitor2: 4, score1: 13, score2: 6, turn: 5}
- {board: 3, competitor1: 9, competitor2: 12, score1: 9, score2: 2, turn: 5}
- {board: 4, competitor1: 1, competitor2: 7, score1: 6, score2: 13, turn: 5}
- {board: 5, competitor1: 2, competitor2: null, score1: 25, score2: 0, turn: 5}
- {board: 6, competitor1: 5, competitor2: 3, score1: 5, score2: 16, turn: 5}
- {board: 7, competitor1: 6, competitor2: 11, score1: 1, score2: 15, turn: 5}
- {board: 1, competitor1: 8, competitor2: 12, score1: 15, score2: 8, turn: 6}
- {board: 2, competitor1: 10, competitor2: 5, score1: 21, score2: 4, turn: 6}
- {board: 3, competitor1: 11, competitor2: 13, score1: 5, score2: 0, turn: 6}
- {board: 4, competitor1: 1, competitor2: 9, score1: 11, score2: 1, turn: 6}
- {board: 5, competitor1: 7, competitor2: 3, score1: 9, score2: 21, turn: 6}
- {board: 6, competitor1: 4, competitor2: null, score1: 25, score2: 0, turn: 6}
- {board: 7, competitor1: 2, competitor2: 6, score1: 9, score2: 8, turn: 6}
- {board: 1, competitor1: 9, competitor2: null, score1: 25, score2: 0, turn: 7}
- {board: 2, competitor1: 11, competitor2: 12, score1: 15, score2: 2, turn: 7}
- {board: 3, competitor1: 6, competitor2: 13, score1: 13, score2: 0, turn: 7}
- {board: 4, competitor1: 1, competitor2: 8, score1: 13, score2: 4, turn: 7}
- {board: 5, competitor1: 10, competitor2: 3, score1: 17, score2: 8, turn: 7}
- {board: 6, competitor1: 4, competitor2: 2, score1: 6, score2: 11, turn: 7}
- {board: 7, competitor1: 7, competitor2: 5, score1: 18, score2: 10, turn: 7}
prized: true
rankedturn: 7
season: 1
---
competitors:
- bucholz: 28
  netscore: 63
  players: [1]
  points: 8
  prize: '16'
  retired: false
  totscore: 82
- bucholz: 24
  netscore: -62
  players: [2]
  points: 2
  prize: '10'
  retired: false
  totscore: 6
- bucholz: 22
  netscore: 88
  players: [3]
  points: 8
  prize: '14'
  retired: false
  totscore: 99
- bucholz: 24
  netscore: -35
  players: [8]
  points: 4
  prize: '11'
  retired: false
  totscore: 28
- bucholz: 30
  netscore: 37
  players: [10]
  points: 8
  prize: '18'
  retired: false
  totscore: 70
- bucholz: 24
  netscore: -23
  players: [11]
  points: 4
  prize: '12'
  retired: false
  totscore: 16
- bucholz: 24
  netscore: -4
  players: [12]
  points: 6
  prize: '13'
  retired: false
  totscore: 45
- bucholz: 24
  netscore: -64
  players: [13]
  points: 0
  prize: '9'
  retired: false
  totscore: 0
couplings: dazed
currentturn: 5
date: 2008-11-29
description: "2\xB0 Torneo"
location: Sede SCR
matches:
- {board: 1, competitor1: 2, competitor2: 3, score1: 0, score2: 25, turn: 1}
- {board: 2, competitor1: 4, competitor2: 8, score1: 10, score2: 0, turn: 1}
- {board: 3, competitor1: 1, competitor2: 6, score1: 19, score2: 0, turn: 1}
- {board: 4, competitor1: 5, competitor2: 7, score1: 17, score2: 3, turn: 1}
- {board: 1, competitor1: 3, competitor2: 4, score1: 25, score2: 0, turn: 2}
- {board: 2, competitor1: 1, competitor2: 5, score1: 7, score2: 13, turn: 2}
- {board: 3, competitor1: 8, competitor2: 2, score1: 0, score2: 5, turn: 2}
- {board: 4, competitor1: 7, competitor2: 6, score1: 9, score2: 0, turn: 2}
- {board: 1, competitor1: 3, competitor2: 5, score1: 23, score2: 4, turn: 3}
- {board: 2, competitor1: 1, competitor2: 2, score1: 25, score2: 0, turn: 3}
- {board: 3, competitor1: 7, competitor2: 4, score1: 12, score2: 8, turn: 3}
- {board: 4, competitor1: 8, competitor2: 6, score1: 0, score2: 8, turn: 3}
- {board: 1, competitor1: 3, competitor2: 1, score1: 1, score2: 7, turn: 4}
- {board: 2, competitor1: 5, competitor2: 4, score1: 25, score2: 0, turn: 4}
- {board: 3, competitor1: 7, competitor2: 8, score1: 16, score2: 0, turn: 4}
- {board: 4, competitor1: 2, competitor2: 6, score1: 0, score2: 8, turn: 4}
- {board: 1, competitor1: 5, competitor2: 6, score1: 11, score2: 0, turn: 5}
- {board: 2, competitor1: 1, competitor2: 7, score1: 24, score2: 5, turn: 5}
- {board: 3, competitor1: 3, competitor2: 8, score1: 25, score2: 0, turn: 5}
- {board: 4, competitor1: 4, competitor2: 2, score1: 10, score2: 1, turn: 5}
prized: true
rankedturn: 5
season: 1
