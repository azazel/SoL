championships:
- {closed: false, club: 1, couplings: serial, description: Campionato CCV 2015-2016,
  guid: 88f630be58e311e5ac012e92809967e5, modified: !!timestamp '2015-09-12 00:16:38',
  owner: 1, playersperteam: 1, previous: 2, prizes: millesimal, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCV 2014-2015,
  guid: 9f05953c0c4211e4ac012e92809967e5, modified: !!timestamp '2015-09-12 00:15:16',
  owner: 1, playersperteam: 1, previous: 3, prizes: millesimal, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCV 2013-2014,
  guid: 9f37d4b0841711e3ac012e92809967e5, modified: !!timestamp '2014-07-15 17:07:32',
  owner: 1, playersperteam: 1, previous: 4, prizes: millesimal, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCV 2012-2013,
  guid: 9f346c08841711e3ac012e92809967e5, modified: !!timestamp '2014-07-15 11:33:40',
  owner: 1, playersperteam: 1, previous: 5, prizes: millesimal, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCV 2011-2012,
  guid: 9f305bcc841711e3ac012e92809967e5, modified: !!timestamp '2014-07-15 11:33:56',
  owner: 1, playersperteam: 1, prizes: millesimal, skipworstprizes: 0}
clubs:
- {couplings: dazed, description: Carrom Club Verona, email: dafattid@cpne.it, guid: 9d72482c841711e3ac012e92809967e5,
  modified: !!timestamp '2015-10-02 16:05:11', nationality: ITA, owner: 1, prizes: fixed}
- {couplings: dazed, description: Italian Carrom Federation, email: info@carromitaly.com,
  emblem: fic.png, guid: 9d70ecb6841711e3ac012e92809967e5, isfederation: true, modified: !!timestamp '2014-01-23
    12:23:02', nationality: ITA, prizes: millesimal, siteurl: 'http://www.carromitaly.com/'}
players:
- birthdate: 1964-02-21
  citizenship: true
  club: 1
  email: dafattid@cpne.it
  federation: 2
  firstname: Daniele
  guid: 9df88f54841711e3ac012e92809967e5
  language: it
  lastname: Da Fatti
  merged:
  - [596799729e7811e3ac012e92809967e5, '', '', '']
  - [8edf915ea08f11e3ac012e92809967e5, Dafatti, Daniele, '']
  modified: 2015-06-07 09:04:44
  nationality: ITA
  nickname: dafattid
  owner: 1
  phone: 00393485813884
  portrait: 8d346ed344d93e9cf503e2e35146dd34.jpeg
  sex: M
- {citizenship: true, club: 1, federation: 2, firstname: Alberto, guid: 9ebd4c36841711e3ac012e92809967e5,
  lastname: Bazzoni, modified: !!timestamp '2014-10-16 08:06:06', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Federico, guid: 7fb77878693711e5ac012e92809967e5,
  language: it, lastname: Bronzato, modified: !!timestamp '2015-10-02 18:57:59', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Davide, guid: b82f3b1c680d11e5ac012e92809967e5,
  language: it, lastname: Cazzola, modified: !!timestamp '2015-10-01 07:26:23', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Fausta, guid: e0cd43b0691c11e5ac012e92809967e5,
  language: it, lastname: Corradi, modified: !!timestamp '2015-10-02 15:47:25', nationality: ITA,
  owner: 1, sex: F}
- {citizenship: true, club: 1, federation: 2, firstname: Martina, guid: 720cdfa2691d11e5ac012e92809967e5,
  language: it, lastname: Domaschio, modified: !!timestamp '2015-10-02 15:51:29',
  nationality: ITA, owner: 1, sex: F}
- {citizenship: true, club: 1, federation: 2, firstname: Giacomo, guid: 9e9a846c841711e3ac012e92809967e5,
  lastname: Gaiga, modified: !!timestamp '2014-10-16 08:10:59', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, firstname: Mattia, guid: 9e84468e841711e3ac012e92809967e5,
  lastname: Gaiga, modified: !!timestamp '2014-08-28 14:02:36', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Sandra, guid: f9e7257c693611e5ac012e92809967e5,
  language: it, lastname: Giori, modified: !!timestamp '2015-10-02 18:54:14', nationality: ITA,
  owner: 1, sex: F}
- {citizenship: false, club: 1, firstname: Keerthi, guid: 9e7f7974841711e3ac012e92809967e5,
  lastname: Ratnayake, modified: !!timestamp '2015-05-03 21:30:42', nationality: ITA,
  owner: 1, portrait: 18a1e91908fa6e17521b54e89648971d.jpeg, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: "Niccol\xF2", guid: 6289eb3eae3711e4ac012e92809967e5,
  language: it, lastname: Sorgato, modified: !!timestamp '2015-03-07 09:24:36', nationality: ITA,
  nickname: insidiasottomar, owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Alvise, guid: 9e91050e841711e3ac012e92809967e5,
  lastname: Toninelli, modified: !!timestamp '2014-10-16 08:09:28', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, firstname: Luca, guid: 9e5c1650841711e3ac012e92809967e5,
  lastname: Toninelli, modified: !!timestamp '2014-08-28 13:57:35', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, club: 1, federation: 2, firstname: Nicola, guid: b0c9cc1e691d11e5ac012e92809967e5,
  language: it, lastname: Voltolina, modified: !!timestamp '2015-10-02 15:53:14',
  nationality: ITA, owner: 1, sex: M}
rates: []
ratings:
- {club: 1, default_deviation: 350, default_rate: 1500, default_volatility: '0.06',
  description: Valutazione Da Fatti Bis, guid: b77a0112691f11e5ac012e92809967e5, higher_rate: 2600,
  inherit: true, level: '4', lower_rate: 1600, modified: !!timestamp '2015-10-02 16:07:54',
  outcomes: guido, owner: 1, tau: '0.5'}
---
championship: 1
competitors:
- bucholz: 0
  netscore: 0
  players: [2]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [3]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [4]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [5]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [1]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [6]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [7]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [8]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [9]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [10]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [11]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [12]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [13]
  points: 0
  totscore: 0
- bucholz: 0
  netscore: 0
  players: [14]
  points: 0
  totscore: 0
couplings: dazed
currentturn: 1
date: 2016-06-02
delaytoppairing: 1
description: "Replica of Campionato CCV - 1\xB0 Tappa"
duration: 25
finalkind: simple
finals: 0
guid: f11a7e4628f611e68a373085a99ccac7
location: San Martino Buon Albergo
matches:
- {board: 1, competitor1: 5, competitor2: 8, final: false, score1: 0, score2: 0, turn: 1}
- {board: 2, competitor1: 10, competitor2: 14, final: false, score1: 0, score2: 0,
  turn: 1}
- {board: 3, competitor1: 1, competitor2: 2, final: false, score1: 0, score2: 0, turn: 1}
- {board: 4, competitor1: 13, competitor2: 11, final: false, score1: 0, score2: 0,
  turn: 1}
- {board: 5, competitor1: 9, competitor2: 12, final: false, score1: 0, score2: 0,
  turn: 1}
- {board: 6, competitor1: 7, competitor2: 3, final: false, score1: 0, score2: 0, turn: 1}
- {board: 7, competitor1: 6, competitor2: 4, final: false, score1: 0, score2: 0, turn: 1}
phantomscore: 25
prealarm: 5
prized: false
rankedturn: 0
rating: 1
