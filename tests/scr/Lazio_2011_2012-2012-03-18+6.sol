championships: []
clubs:
- {couplings: dazed, description: Federazione Italiana Carrom, emblem: fic.png, guid: a67d9b0c6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 12:09:06', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromitaly.com/'}
players:
- {citizenship: true, club: 1, firstname: Rodolfo, guid: a692707c6af811e3bee53085a99ccac7,
  lastname: Donnini, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  portrait: rodolfo.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Ari, guid: a69307626af811e3bee53085a99ccac7,
  lastname: Liyanage, modified: !!timestamp '2013-12-22 12:09:07', nationality: LKA,
  portrait: ary.jpg}
- {citizenship: true, club: 1, firstname: Paolo, guid: a69399526af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  portrait: Paolomart.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Stefano, guid: a695f60c6af811e3bee53085a99ccac7,
  lastname: Stucchi, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  portrait: Stucchi.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Lal, guid: a69688426af811e3bee53085a99ccac7,
  lastname: Rodrigo, modified: !!timestamp '2013-12-22 12:09:07', nationality: LKA,
  portrait: lal.jpg}
- {citizenship: true, club: 1, firstname: Amitha, guid: a6a942ac6af811e3bee53085a99ccac7,
  lastname: Ubhayathunga, modified: !!timestamp '2013-12-22 12:09:07', nationality: LKA,
  portrait: amitha.jpg}
- {citizenship: true, club: 1, firstname: Elisa, guid: a6b21d326af811e3bee53085a99ccac7,
  lastname: Zucchiatti, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  nickname: elisa, portrait: elisa.jpg, sex: F}
- {citizenship: true, club: 1, firstname: Patrick, guid: a70b8b9c6af811e3bee53085a99ccac7,
  lastname: Philips, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA}
- {citizenship: true, club: 1, firstname: Giorgio, guid: a71151bc6af811e3bee53085a99ccac7,
  lastname: Balicchi, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Francesco, guid: a718d4506af811e3bee53085a99ccac7,
  lastname: Pedicini, modified: !!timestamp '2013-12-22 12:09:07', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Sumith, guid: a750c7b66af811e3bee53085a99ccac7,
  lastname: Wickrama, modified: !!timestamp '2013-12-22 12:09:08', nationality: LKA}
- citizenship: true
  club: 1
  firstname: Lucia Elena
  guid: a7fe84a06af811e3bee53085a99ccac7
  lastname: Ammazzalamorte
  merged: [a8a543946af811e3bee53085a99ccac7]
  modified: 2013-12-22 12:09:53
  nationality: ITA
  sex: F
- {citizenship: true, firstname: Luigi, guid: f73a95286b0111e3bf0b3085a99ccac7, lastname: Cartolano,
  modified: !!timestamp '2013-12-22 12:09:53', nationality: ITA, sex: M}
- {citizenship: true, firstname: Emiliano, guid: f73b0b8e6b0111e3bf0b3085a99ccac7,
  lastname: Mazzone, modified: !!timestamp '2013-12-22 12:09:53', nationality: ITA,
  sex: M}
- {citizenship: true, firstname: Lakmal, guid: f73d18166b0111e3bf0b3085a99ccac7, lastname: Chaturanga,
  modified: !!timestamp '2013-12-22 12:09:53', sex: M}
rates: []
ratings: []
seasons:
- {closed: true, club: 1, couplings: serial, description: Lazio 2011-2012, guid: a926de046af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 12:09:55', playersperteam: 1, prizefactor: '0',
  prizes: millesimal, skipworstprizes: 1}
---
competitors:
- bucholz: 34
  netscore: 28
  players: [1]
  points: 6
  prize: '472'
  totscore: 99
- bucholz: 30
  netscore: 29
  players: [2]
  points: 6
  prize: '406'
  totscore: 83
- bucholz: 36
  netscore: 45
  players: [3]
  points: 8
  prize: '736'
  totscore: 113
- bucholz: 38
  netscore: 57
  players: [4]
  points: 10
  prize: '934'
  totscore: 117
- bucholz: 36
  netscore: 33
  players: [5]
  points: 8
  prize: '670'
  totscore: 116
- bucholz: 44
  netscore: 55
  players: [6]
  points: 8
  prize: '802'
  totscore: 120
- bucholz: 46
  netscore: -16
  players: [7]
  points: 6
  prize: '604'
  totscore: 77
- bucholz: 40
  netscore: 41
  players: [8]
  points: 10
  prize: '1000'
  totscore: 95
- bucholz: 36
  netscore: 8
  players: [9]
  points: 6
  prize: '538'
  totscore: 92
- bucholz: 46
  netscore: 62
  players: [10]
  points: 8
  prize: '868'
  totscore: 116
- bucholz: 40
  netscore: -43
  players: [11]
  points: 4
  prize: '208'
  totscore: 64
- bucholz: 30
  netscore: -87
  players: [12]
  points: 2
  prize: '76'
  totscore: 37
- bucholz: 24
  netscore: -25
  players: [13]
  points: 4
  prize: '142'
  totscore: 71
- bucholz: 22
  netscore: -2
  players: [14]
  points: 6
  prize: '340'
  totscore: 73
- bucholz: 42
  netscore: -35
  players: [15]
  points: 4
  prize: '274'
  totscore: 83
couplings: serial
currentturn: 6
date: 2012-03-18
description: 3 tappa Roma
duration: 45
guid: 016584186b0211e3bf0b3085a99ccac7
location: Esquilino
matches:
- {board: 1, competitor1: 2, competitor2: 8, score1: 2, score2: 16, turn: 1}
- {board: 2, competitor1: 14, competitor2: 15, score1: 8, score2: 25, turn: 1}
- {board: 3, competitor1: 7, competitor2: 9, score1: 13, score2: 7, turn: 1}
- {board: 4, competitor1: 4, competitor2: 1, score1: 16, score2: 8, turn: 1}
- {board: 5, competitor1: 12, competitor2: 13, score1: 0, score2: 25, turn: 1}
- {board: 6, competitor1: 6, competitor2: 3, score1: 25, score2: 9, turn: 1}
- {board: 7, competitor1: 11, competitor2: 10, score1: 0, score2: 25, turn: 1}
- {board: 8, competitor1: 5, competitor2: null, score1: 25, score2: 0, turn: 1}
- {board: 1, competitor1: 5, competitor2: 10, score1: 15, score2: 21, turn: 2}
- {board: 2, competitor1: 6, competitor2: 8, score1: 11, score2: 14, turn: 2}
- {board: 3, competitor1: 13, competitor2: 15, score1: 13, score2: 25, turn: 2}
- {board: 4, competitor1: 2, competitor2: 3, score1: 6, score2: 23, turn: 2}
- {board: 5, competitor1: 4, competitor2: 7, score1: 15, score2: 19, turn: 2}
- {board: 6, competitor1: 9, competitor2: 1, score1: 25, score2: 11, turn: 2}
- {board: 7, competitor1: 12, competitor2: null, score1: 25, score2: 0, turn: 2}
- {board: 8, competitor1: 14, competitor2: 11, score1: 4, score2: 25, turn: 2}
- {board: 1, competitor1: 13, competitor2: 6, score1: 0, score2: 25, turn: 3}
- {board: 2, competitor1: 10, competitor2: 7, score1: 25, score2: 1, turn: 3}
- {board: 3, competitor1: 4, competitor2: 11, score1: 25, score2: 6, turn: 3}
- {board: 4, competitor1: 15, competitor2: 8, score1: 0, score2: 24, turn: 3}
- {board: 5, competitor1: 5, competitor2: 9, score1: 25, score2: 2, turn: 3}
- {board: 6, competitor1: 2, competitor2: 14, score1: 0, score2: 8, turn: 3}
- {board: 7, competitor1: 1, competitor2: null, score1: 25, score2: 0, turn: 3}
- {board: 8, competitor1: 3, competitor2: 12, score1: 25, score2: 4, turn: 3}
- {board: 1, competitor1: 4, competitor2: 3, score1: 20, score2: 6, turn: 4}
- {board: 2, competitor1: 11, competitor2: 13, score1: 21, score2: 7, turn: 4}
- {board: 3, competitor1: 10, competitor2: 8, score1: 19, score2: 1, turn: 4}
- {board: 4, competitor1: 6, competitor2: 7, score1: 21, score2: 11, turn: 4}
- {board: 5, competitor1: 9, competitor2: 12, score1: 25, score2: 8, turn: 4}
- {board: 6, competitor1: 15, competitor2: 5, score1: 18, score2: 23, turn: 4}
- {board: 7, competitor1: 1, competitor2: 14, score1: 25, score2: 4, turn: 4}
- {board: 8, competitor1: 2, competitor2: null, score1: 25, score2: 0, turn: 4}
- {board: 1, competitor1: 7, competitor2: 11, score1: 21, score2: 10, turn: 5}
- {board: 2, competitor1: 4, competitor2: 9, score1: 25, score2: 8, turn: 5}
- {board: 3, competitor1: 3, competitor2: 15, score1: 25, score2: 8, turn: 5}
- {board: 4, competitor1: 14, competitor2: null, score1: 25, score2: 0, turn: 5}
- {board: 5, competitor1: 10, competitor2: 6, score1: 13, score2: 21, turn: 5}
- {board: 6, competitor1: 2, competitor2: 12, score1: 25, score2: 0, turn: 5}
- {board: 7, competitor1: 8, competitor2: 5, score1: 25, score2: 10, turn: 5}
- {board: 8, competitor1: 1, competitor2: 13, score1: 25, score2: 1, turn: 5}
- {board: 1, competitor1: 14, competitor2: 12, score1: 24, score2: 0, turn: 6}
- {board: 2, competitor1: 3, competitor2: 1, score1: 25, score2: 5, turn: 6}
- {board: 3, competitor1: 6, competitor2: 5, score1: 17, score2: 18, turn: 6}
- {board: 4, competitor1: 9, competitor2: 11, score1: 25, score2: 2, turn: 6}
- {board: 5, competitor1: 15, competitor2: 2, score1: 7, score2: 25, turn: 6}
- {board: 6, competitor1: 10, competitor2: 4, score1: 13, score2: 16, turn: 6}
- {board: 7, competitor1: 13, competitor2: null, score1: 25, score2: 0, turn: 6}
- {board: 8, competitor1: 8, competitor2: 7, score1: 15, score2: 12, turn: 6}
modified: 2013-12-22 12:10:10
prealarm: 5
prized: true
prizefactor: '1'
prizes: millesimal
rankedturn: 6
season: 1
