# -*- mode: makefile; coding: utf-8 -*-
# :Project:   SoL -- Release related targets
# :Created:   lun 3 mar 2014 11:15:10 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2014, 2016, 2018, 2019 Lele Gaifax
#

BUMPER := $(BINDIR)bump_version --scheme pep440
VERSION_TXT := version.txt
VERSION = $(shell cat $(VERSION_TXT))
MANIFEST := manifest.json
define MEDPRSTMT
from pkg_resources import resource_filename as rfn;
print(rfn('metapensiero.extjs.desktop',''))
endef
MEDSRC := $(shell $(PYTHON) -c "$(MEDPRSTMT)")
PACKED_CSS := $(SOLSRC)/static/all-styles.css
PACKED_JS := $(SOLSRC)/static/all-classes.js
MINIFIER := $(BINDIR)minify_js_scripts \
		--output-js $(PACKED_JS) \
		--output-css $(PACKED_CSS) \
		--prefix-map /static=$(SOLSRC)/static \
		--prefix-map /desktop=$(MEDSRC)/assets \
		--prefix-map SoL=$(SOLSRC)/static/app \
		--prefix-map MP=$(MEDSRC)/assets/js \
		--prefix-map Ext=$(MEDSRC)/assets/extjs/src \
		--extjs-auto-deps \
		--extjs-core-bundle $(MEDSRC)/assets/extjs/ext-dev.js \
		$(MANIFEST)
YUICOMPRESSOR := $(BINDIR)yuicompressor
SOL_INIT = $(SOLSRC)/__init__.py

.PHONY: bump-beta-version
bump-beta-version:
	$(BUMPER) --field pre $(VERSION_TXT)

.PHONY: bump-minor-version
bump-minor-version:
	$(BUMPER) --field release --index 1 $(VERSION_TXT)

.PHONY: bump-major-version
bump-major-version:
	$(BUMPER) --field release --index 0 $(VERSION_TXT)

.PHONY: assert-clean-tree
assert-clean-tree:
	@(test -z "$(shell git status -s --untracked=no)" || \
	  (echo "UNCOMMITTED STUFF" && false))

.PHONY: assert-master-branch
assert-master-branch:
	@(test "$(shell git rev-parse --abbrev-ref HEAD)" = "master" || \
	  (echo "NOT IN MASTER BRANCH" && false))

.PHONY: beta-release
beta-release: assert-master-branch assert-clean-tree
	$(MAKE) bump-beta-version update-POs-version update-catalogs
	@echo ">>>"
	@echo ">>> Do your duties (update CHANGES.rst for example), then"
	@echo ">>> execute “make tag-release”."
	@echo ">>>"

.PHONY: release
release: assert-master-branch assert-clean-tree
	$(MAKE) bump-minor-version update-POs-version update-catalogs
	@echo ">>>"
	@echo ">>> Do your duties (update CHANGES.rst for example), then"
	@echo ">>> execute “make tag-release”."
	@echo ">>>"

.PHONY: major-release
major-release: assert-master-branch assert-clean-tree
	$(MAKE) bump-major-version update-POs-version update-catalogs
	@echo ">>>"
	@echo ">>> Do your duties (update CHANGES.rst for example), then"
	@echo ">>> execute “make tag-release”."
	@echo ">>>"

.PHONY: tag-release
tag-release: assert-master-branch check-release-date check-long-description-markup requirements/versions.cfg
	sed -i "s/__exact_version__ = '.*'/__exact_version__ = 'v$(VERSION)'/" $(SOL_INIT)
	git commit -a -m "Release $(VERSION)"
	git tag -a -m "Version $(VERSION)" v$(VERSION)

.PHONY: check-long-description-markup
check-long-description-markup:
	@$(PYTHON) setup.py check -r -s

.PHONY: check-release-date
check-release-date:
	@fgrep -q "$(VERSION) ($(shell date --iso-8601))" CHANGES.rst \
	  || (echo "ERROR: release date of version $(VERSION) not set in CHANGES.rst"; exit 1)

requirements/versions.cfg: requirements/base.txt $(VERSION_TXT)
	@echo "[versions]" > $@
	@sed 's/==/ = /' $< >> $@
	@echo 'SoL = $(VERSION)' >> $@

.PHONY: dist
dist: clean compile-catalogs manual minimize

.PHONY: minimize
minimize: $(SOLSRC)/static/ext.js
	$(MINIFIER)

$(SOLSRC)/static/ext.js: $(MEDSRC)/assets/extjs/ext-dev.js
	$(YUICOMPRESSOR) $< -o $@

.PHONY: pypi-upload
pypi-upload: assert-master-branch assert-clean-tree dist
	$(PYTHON) setup.py sdist bdist_wheel upload

.PHONY: publish
publish: pypi-upload
	git push --follow-tags
	rm -rf build
