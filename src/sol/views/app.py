# -*- coding: utf-8 -*-
# :Project:   SoL -- Override default mp.extjs.desktop app view
# :Created:   sab 14 lug 2018 11:26:09 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Lele Gaifax
#

from pyramid.view import view_config
from pyramid.settings import asbool


@view_config(route_name='app', renderer='app.mako')
def app_view(request):
    from metapensiero.extjs.desktop.pyramid import app_view
    data = app_view(request)
    data['locale'] = getattr(request, '_LOCALE_', 'en')
    settings = request.registry.settings
    data['signin_enabled'] = asbool(settings.get('sol.enable_signin'))
    data['password_reset_enabled'] = asbool(settings.get('sol.enable_password_reset'))
    return data
